package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class CabinetFactory {
	static Cabinet createCabinetWithPlentyOfCapacity() {
		LockerSetting[] settings =
			Arrays.stream(LockerSize.values()).map(size -> LockerSetting.of(
				size,
				Integer.MAX_VALUE)).toArray(LockerSetting[]::new);
		return new Cabinet(settings);
	}
	
	static Cabinet createCabinetWithFullLockers(LockerSize[] fullLockers, int defaultCapacity) {
		LockerSetting[] setting =
			Arrays.stream(LockerSize.values()).map(size -> LockerSetting.of(size, defaultCapacity))
				.toArray(LockerSetting[]::new);
		
		Cabinet cabinet = new Cabinet(setting);
		for (LockerSize lockerSize : fullLockers) {
			for (int i = 0; i < defaultCapacity; i++) {
				cabinet.save(new Bag(getBagSizeFromLockerSize(lockerSize)), lockerSize);
			}
		}
		
		return cabinet;
	}
	
	static List<Cabinet> createCabinetListWithPlentyOfCapacity(int num) {
		LockerSetting[] settings =
			Arrays.stream(LockerSize.values()).map(size -> LockerSetting.of(
				size,
				Integer.MAX_VALUE)).toArray(LockerSetting[]::new);
		List<Cabinet> cabinets = new ArrayList<>();
		for (int i = 0; i < num; i++) {
			cabinets.add( new Cabinet(settings));
		}
		
		return cabinets;
	}
	
	private static BagSize getBagSizeFromLockerSize(LockerSize lockerSize) {
		return BagSize.valueOf(lockerSize.toString());
	}
}
