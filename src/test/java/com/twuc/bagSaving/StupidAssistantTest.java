package com.twuc.bagSaving;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

import static com.twuc.bagSaving.CabinetFactory.createCabinetListWithPlentyOfCapacity;
import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

public class StupidAssistantTest {
	
	@ParameterizedTest
	@MethodSource("com.twuc.bagSaving.BagSavingArgument#createSavableBagSizeAndLockerSize")
	void should_save_and_get_bag_by_assistant(BagSize bagSize, LockerSize lockerSize) {
		Cabinet cabinet = createCabinetWithPlentyOfCapacity();
		Bag savedBag = new Bag(bagSize);
		StupidAssistant stupidAssistant = new StupidAssistant();
		
		Ticket ticket = stupidAssistant.saveBag(savedBag, lockerSize, cabinet);
		Bag getBag = stupidAssistant.getBag(ticket, cabinet);
		
		assertSame(savedBag, getBag);
	}
	
	@ParameterizedTest
	@MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneLockerSizeFull")
	void should_throw_when_lockers_are_full(
		Cabinet fullCabinet, BagSize bagSize, LockerSize lockerSize) {
		Bag savedBag = new Bag(bagSize);
		StupidAssistant stupidAssistant = new StupidAssistant();
		
		InsufficientLockersException exception = assertThrows(
			InsufficientLockersException.class,
			() -> stupidAssistant.saveBag(savedBag, lockerSize, fullCabinet));
		
		assertEquals("Insufficient empty lockers.", exception.getMessage());
	}
	
}
