package com.twuc.bagSaving;

import java.util.List;

public class StupidAssistant {
	
	public Ticket saveBag(Bag bag, LockerSize lockerSize, Cabinet cabinet) {
		return cabinet.save(bag, lockerSize);
	}
	
	public Ticket saveBag(Bag bag, LockerSize lockerSize, List<Cabinet> cabinets) {

//		cabinets.stream().map(item->{
//
//		});

		return cabinets.get(0).save(bag, lockerSize);
	}
	
	public Bag getBag(Ticket ticket, Cabinet cabinet) {
		return cabinet.getBag(ticket);
	}
	
	public Bag getBag(Ticket ticket, List<Cabinet> cabinets) {
		
		return cabinets.get(0).getBag(ticket);
	}
}
